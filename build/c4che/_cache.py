BINDIR = '/usr/local/bin'
BLOCK_MESSAGE_KEYS = []
BUILD_TYPE = 'app'
BUNDLE_NAME = 'rj45-pbwa.pbw'
DEFINES = ['RELEASE']
LIBDIR = '/usr/local/lib'
LIB_DIR = 'node_modules'
LIB_JSON = []
MESSAGE_KEYS = {u'dummy': 10000}
MESSAGE_KEYS_DEFINITION = '/home/rebble/Documents/rj45-pbwa/build/src/message_keys.auto.c'
MESSAGE_KEYS_HEADER = '/home/rebble/Documents/rj45-pbwa/build/include/message_keys.auto.h'
MESSAGE_KEYS_JSON = '/home/rebble/Documents/rj45-pbwa/build/js/message_keys.json'
NODE_PATH = '/home/rebble/.pebble-sdk/SDKs/current/node_modules'
PEBBLE_SDK_COMMON = '/home/rebble/.pebble-sdk/SDKs/current/sdk-core/pebble/common'
PEBBLE_SDK_ROOT = '/home/rebble/.pebble-sdk/SDKs/current/sdk-core/pebble'
PREFIX = '/usr/local'
PROJECT_INFO = {'appKeys': {u'dummy': 10000}, u'watchapp': {u'watchface': False}, u'displayName': u'Pinout', u'uuid': u'0d06b275-ee27-4cf5-8cbe-c0fbf729b7b9', u'messageKeys': {u'dummy': 10000}, 'companyName': u'Durrendal', u'enableMultiJS': True, u'sdkVersion': u'3', 'versionLabel': u'1.0', u'targetPlatforms': [u'basalt', u'chalk'], 'longName': u'Pinout', 'shortName': u'Pinout', u'resources': {u'media': [{u'type': u'bitmap', u'name': u'RJ45B', u'file': u'RJ45B.png'}]}, 'name': u'Pinout'}
REQUESTED_PLATFORMS = [u'basalt', u'chalk']
RESOURCES_JSON = [{u'type': u'bitmap', u'name': u'RJ45B', u'file': u'RJ45B.png'}]
SANDBOX = False
SUPPORTED_PLATFORMS = ['basalt', 'aplite', 'emery', 'diorite', 'chalk']
TARGET_PLATFORMS = ['chalk', 'basalt']
TIMESTAMP = 1741323749
USE_GROUPS = True
VERBOSE = 0
WEBPACK = '/home/rebble/.pebble-sdk/SDKs/current/node_modules/.bin/webpack'
